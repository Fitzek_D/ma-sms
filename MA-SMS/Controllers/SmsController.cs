﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MA_SMS.Controllers
{
    [Route("api/[controller]/[action]")]
    public class SmsController : Controller
    {
        [HttpPost]
        [ActionName("send-erinnerung")]
        public ActionResult SendErinnerung([FromBody] SmsDto smsData)
        {
            var sms = "Hallo " + smsData.Name + ". Sie müssen noch Ihre Zeiten eintragen!";

            var smsServiceMessage = "Die SMS an die Nummer: " + smsData.Handynummer + " wurde erfolgreich gesendet. Die Nachricht lautete: " + sms;

            Console.WriteLine(smsServiceMessage);

            return Ok(smsServiceMessage);
        }

        [HttpPost]
        [ActionName("send-lohnabrechnung")]
        public ActionResult SendLohnabrechnung([FromBody] SmsDto smsData)
        {
            var sms = "Hallo " + smsData.Name + ". Eine neue Lohnabrechung wurde Ihnen auf Ihre Email-Adresse gesendet.";

            var smsServiceMessage = "Die SMS an die Nummer: " + smsData.Handynummer + " wurde erfolgreich gesendet. Die Nachricht lautete: " + sms;

            Console.WriteLine(smsServiceMessage);

            return Ok(smsServiceMessage);
        }

    }
}
